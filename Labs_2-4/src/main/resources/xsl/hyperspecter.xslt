<?xml version="1.0" ?>
<xsl:stylesheet
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        version="3.0"
>
    <xsl:output method="html" indent="yes" media-type="text/html" encoding="UTF-8" />
    <xsl:template match="/">
        <html>
            <head>
                <title>API XSLT</title>
                <body>
                    <h1>Test XSLT Transformation</h1>
                    <xsl:apply-templates />
                </body>
            </head>
            <style>
                table, th, td {
                border: 1px solid black;
                }
            </style>
        </html>
    </xsl:template>
    <xsl:template match="ArrayList">
        <h2>List of hyperspecters</h2>
        <table>
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Description</th>
                <th>Height</th>
                <th>Width</th>
                <th>LayersCount</th>
                <th>R</th>
                <th>G</th>
                <th>B</th>
                <th>Username</th>
                <th>Visible</th>
            </tr>
            <xsl:for-each select="item">
                <tr>
                    <td><xsl:value-of select="name"/></td>
                    <td><xsl:value-of select="type"/></td>
                    <td><xsl:value-of select="description"/></td>
                    <td><xsl:value-of select="height"/></td>
                    <td><xsl:value-of select="width"/></td>
                    <td><xsl:value-of select="layersCount"/></td>
                    <td><xsl:value-of select="r"/></td>
                    <td><xsl:value-of select="g"/></td>
                    <td><xsl:value-of select="b"/></td>
                    <td><xsl:value-of select="username"/></td>
                    <td><xsl:value-of select="visible"/></td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
</xsl:stylesheet>