package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> findById(Integer id);
    List<User> findAll();
    Optional<User> findByUsername(String username);
    void save(User user);
    void delete(User user);
}
