package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.aspects.NoLogging;
import com.banayaki.lab2_spring.models.WatcherEvent;
import com.banayaki.lab2_spring.repository.WatcherEventRepository;
import com.banayaki.lab2_spring.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Repository
public class WatcherServiceImpl implements WatcherEventService {

    private final WatcherEventRepository repository;

    @Autowired
    public WatcherServiceImpl(WatcherEventRepository repository) {
        this.repository = repository;
    }

    @Override
    @NoLogging
    public Optional<WatcherEvent> findById(Integer id) {
        return repository.findById(id);
    }

    @Override
    @NoLogging
    public List<WatcherEvent> findAll() {
        return Converter.iterableToArrayList(repository.findAll());
    }

    @Override
    @NoLogging
    public void save(WatcherEvent watcherEvent) {
        repository.save(watcherEvent);
    }

    @Override
    @NoLogging
    public void delete(WatcherEvent watcherEvent) {
        repository.delete(watcherEvent);
    }
}
