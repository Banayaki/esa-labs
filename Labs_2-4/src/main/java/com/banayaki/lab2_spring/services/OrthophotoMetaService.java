package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.models.OrthophotoMeta;

import java.util.List;
import java.util.Optional;

public interface OrthophotoMetaService {
    Optional<OrthophotoMeta> findById(Integer id);
    List<OrthophotoMeta> findAll();
    void save(OrthophotoMeta computedLayer);
    void delete(OrthophotoMeta computedLayer);
    Optional<OrthophotoMeta> findByHyperspecterId(Integer id);
}
