package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.models.ComputedLayer;
import com.banayaki.lab2_spring.repository.ComputedLayerRepository;
import com.banayaki.lab2_spring.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Repository
@Transactional
public class ComputedLayerServiceImpl implements ComputedLayerService{

    private final ComputedLayerRepository repository;

    @Autowired
    public ComputedLayerServiceImpl(ComputedLayerRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<ComputedLayer> findById(Integer id) {
        return repository.findById(id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<ComputedLayer> findAll() {
        return Converter.iterableToArrayList(repository.findAll());
    }

    @Transactional(readOnly = true)
    @Override
    public List<ComputedLayer> findByHyperspecterId(Integer id) {
        return repository.findByHyperspecter_Id(id);
    }

    @Override
    public void save(ComputedLayer computedLayer) {
        repository.save(computedLayer);
    }

    @Override
    public void delete(ComputedLayer computedLayer) {
        repository.delete(computedLayer);
    }
}
