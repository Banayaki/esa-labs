package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.aspects.NoLogging;
import com.banayaki.lab2_spring.models.Email;
import com.banayaki.lab2_spring.repository.EmailRepository;
import com.banayaki.lab2_spring.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Repository
@Transactional
public class EmailServiceImpl implements EmailService {

    private final EmailRepository repository;

    @Autowired
    public EmailServiceImpl(EmailRepository repository) {
        this.repository = repository;
    }

    @Override
    @NoLogging
    public Optional<Email> findById(Integer id) {
        return repository.findById(id);
    }

    @Override
    @NoLogging
    public List<Email> findAll() {
        return Converter.iterableToArrayList(repository.findAll());
    }

    @Override
    @NoLogging
    public void save(Email email) {
        repository.save(email);
    }

    @Override
    @NoLogging
    public void delete(Email email) {
        repository.delete(email);
    }
}
