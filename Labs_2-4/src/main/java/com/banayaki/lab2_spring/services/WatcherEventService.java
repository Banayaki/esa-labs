package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.models.WatcherEvent;

import java.util.List;
import java.util.Optional;

public interface WatcherEventService {
    Optional<WatcherEvent> findById(Integer id);
    List<WatcherEvent> findAll();
    void save(WatcherEvent watcherEvent);
    void delete(WatcherEvent watcherEvent);
}
