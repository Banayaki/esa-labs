package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.models.Email;

import java.util.List;
import java.util.Optional;

public interface EmailService {
    Optional<Email> findById(Integer id);
    List<Email> findAll();
    void save(Email email);
    void delete(Email email);
}
