package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.models.ComputedLayer;

import java.util.List;
import java.util.Optional;

public interface ComputedLayerService {
    Optional<ComputedLayer> findById(Integer id);
    List<ComputedLayer> findAll();
    void save(ComputedLayer computedLayer);
    void delete(ComputedLayer computedLayer);
    List<ComputedLayer> findByHyperspecterId(Integer id);
}
