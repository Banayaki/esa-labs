package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.jms.EmailListener;
import com.banayaki.lab2_spring.jms.EventLoggerListener;
import com.banayaki.lab2_spring.jms.EventManager;
import com.banayaki.lab2_spring.jms.ListenerFactory;
import com.banayaki.lab2_spring.models.Hyperspecter;
import com.banayaki.lab2_spring.models.WatcherEvent;
import com.banayaki.lab2_spring.repository.HypterpsecterRepository;
import com.banayaki.lab2_spring.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.banayaki.lab2_spring.models.EventType.*;

@Service
@Repository
@Transactional
public class HyperspecterServiceImpl implements HyperspecterService {

    private final EventManager eventManager;
    private final HypterpsecterRepository repository;
    private final ListenerFactory listenerFactory;

    @Autowired
    public HyperspecterServiceImpl(HypterpsecterRepository repository, ListenerFactory listenerFactory) {
        this.repository = repository;
        this.listenerFactory = listenerFactory;
        this.eventManager = new EventManager("get", "save", "delete");
        this.eventManager.subscribe("get", listenerFactory.createEventLoggerListener());
        this.eventManager.subscribe("save", listenerFactory.createEmailListener());
        this.eventManager.subscribe("save", listenerFactory.createEventLoggerListener());
        this.eventManager.subscribe("delete", listenerFactory.createEmailListener());
        this.eventManager.subscribe("delete", listenerFactory.createEventLoggerListener());
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<Hyperspecter> findById(Integer id) {
        return repository.findById(id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Hyperspecter> findAll() {
        WatcherEvent event = new WatcherEvent(SELECT_TYPE, "Hyperspecter", "");
        this.eventManager.notify("get", event);
        return Converter.iterableToArrayList(repository.findAll());
    }

    @Transactional(readOnly = true)
    @Override
    public List<Hyperspecter> findByUsername(String username) {
        return repository.findByUser_Username(username);
    }

    @Override
    public void save(Hyperspecter hyperspecter) {
        WatcherEvent event = new WatcherEvent(INSERT_TYPE, "Hyperspecter", hyperspecter.toString());
        this.eventManager.notify("save", event);
        repository.save(hyperspecter);
    }

    @Override
    public void delete(Hyperspecter hyperspecter) {
        WatcherEvent event = new WatcherEvent(DELETE_TYPE, "Hyperspecter", hyperspecter.toString());
        this.eventManager.notify("delete", event);
        repository.delete(hyperspecter);
    }
}
