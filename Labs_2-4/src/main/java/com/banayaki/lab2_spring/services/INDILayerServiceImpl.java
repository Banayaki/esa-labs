package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.models.INDILayer;
import com.banayaki.lab2_spring.repository.INDILayerRepository;
import com.banayaki.lab2_spring.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Repository
@Transactional
public class INDILayerServiceImpl implements INDILayerService {

    private final INDILayerRepository repository;

    @Autowired
    public INDILayerServiceImpl(INDILayerRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<INDILayer> findById(Integer id) {
        return repository.findById(id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<INDILayer> findAll() {
        return Converter.iterableToArrayList(repository.findAll());
    }

    @Transactional(readOnly = true)
    @Override
    public List<INDILayer> findByHyperspecterId(Integer id) {
        return repository.findByHyperspecter_Id(id);
    }

    @Override
    public void save(INDILayer layer) {
        repository.save(layer);
    }

    @Override
    public void delete(INDILayer layer) {
        repository.delete(layer);
    }
}
