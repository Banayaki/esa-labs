package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.models.Hyperspecter;

import java.util.List;
import java.util.Optional;

public interface HyperspecterService {
    Optional<Hyperspecter> findById(Integer id);
    List<Hyperspecter> findAll();
    void save(Hyperspecter computedLayer);
    void delete(Hyperspecter computedLayer);
    List<Hyperspecter> findByUsername(String username);
}
