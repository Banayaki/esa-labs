package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.models.INDILayer;

import java.util.List;
import java.util.Optional;

public interface INDILayerService {
    Optional<INDILayer> findById(Integer id);
    List<INDILayer> findAll();
    void save(INDILayer computedLayer);
    void delete(INDILayer computedLayer);
    List<INDILayer> findByHyperspecterId(Integer id);
}
