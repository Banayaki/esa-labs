package com.banayaki.lab2_spring.services;

import com.banayaki.lab2_spring.models.OrthophotoMeta;
import com.banayaki.lab2_spring.repository.OrthophotoMetaRepository;
import com.banayaki.lab2_spring.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Repository
@Transactional
public class OrthophotoServiceImpl implements OrthophotoMetaService {

    private final OrthophotoMetaRepository repository;

    @Autowired
    public OrthophotoServiceImpl(OrthophotoMetaRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<OrthophotoMeta> findById(Integer id) {
        return repository.findById(id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<OrthophotoMeta> findAll() {
        return Converter.iterableToArrayList(repository.findAll());
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<OrthophotoMeta> findByHyperspecterId(Integer id) {
        return repository.findByHyperspecter_Id(id);
    }

    @Override
    public void save(OrthophotoMeta meta) {
        repository.save(meta);
    }

    @Override
    public void delete(OrthophotoMeta meta) {
        repository.delete(meta);
    }
}
