//package com.banayaki.lab2_spring.aspects;
//
//import com.banayaki.lab2_spring.models.Email;
//import com.banayaki.lab2_spring.models.EventType;
//import com.banayaki.lab2_spring.models.WatcherEvent;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.annotation.After;
//import org.aspectj.lang.annotation.AfterReturning;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jms.core.JmsTemplate;
//import org.springframework.stereotype.Component;
//
//import java.util.Arrays;
//import java.util.Optional;
//
//@Aspect
//@Component
//public class LoggingAspect {
//
//    @Autowired
//    JmsTemplate jmsTemplate;
//
//    @AfterReturning(value = "within(com.banayaki.lab2_spring.services.*) && !@annotation(NoLogging)", returning = "returnValue")
//    public void logAfterExecuteCrud(JoinPoint joinPoint, Object returnValue) {
//        String methodName = joinPoint.getSignature().getName();
//        String[] packageSplitted = joinPoint.getTarget().getClass().getName().split("\\.", 0);
//        String targetObject = packageSplitted[packageSplitted.length - 1].split("ServiceImpl")[0];
//        String eventType = null;
//        String arguments = Arrays.toString(joinPoint.getArgs());
//
//        if (methodName.contains("find")) {
//            eventType = EventType.SELECT_TYPE;
//            if (returnValue instanceof Optional) {
//                if (!((Optional<?>) returnValue).isPresent()) {
//                    String msg = "Someone is trying to access to an nonexistent property";
//                    jmsTemplate.convertAndSend("mail", new Email("admin", msg));
//                }
//            }
//        } else if (methodName.equals("save")) {
//            eventType = EventType.INSERT_TYPE;
//        } else if (methodName.equals("delete")) {
//            eventType = EventType.DELETE_TYPE;
//        }
//
//        jmsTemplate.convertAndSend("event", new WatcherEvent(eventType, targetObject, arguments));
//    }
//}
