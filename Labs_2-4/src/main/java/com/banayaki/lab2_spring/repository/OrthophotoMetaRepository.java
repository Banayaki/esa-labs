package com.banayaki.lab2_spring.repository;

import com.banayaki.lab2_spring.models.OrthophotoMeta;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface OrthophotoMetaRepository extends CrudRepository<OrthophotoMeta, Integer> {
    Optional<OrthophotoMeta> findByHyperspecter_Id(Integer id);
}
