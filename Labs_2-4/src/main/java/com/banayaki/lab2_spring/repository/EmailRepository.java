package com.banayaki.lab2_spring.repository;

import com.banayaki.lab2_spring.models.Email;
import org.springframework.data.repository.CrudRepository;

public interface EmailRepository extends CrudRepository<Email, Integer> {
}