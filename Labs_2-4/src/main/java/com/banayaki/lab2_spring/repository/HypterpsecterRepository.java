package com.banayaki.lab2_spring.repository;

import com.banayaki.lab2_spring.models.Hyperspecter;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface HypterpsecterRepository extends CrudRepository<Hyperspecter, Integer> {
    List<Hyperspecter> findByUser_Username(String username);
}
