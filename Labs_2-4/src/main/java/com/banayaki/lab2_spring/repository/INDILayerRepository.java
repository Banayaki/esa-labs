package com.banayaki.lab2_spring.repository;

import com.banayaki.lab2_spring.models.INDILayer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface INDILayerRepository extends CrudRepository<INDILayer, Integer> {
    List<INDILayer> findByHyperspecter_Id(Integer id);
}
