package com.banayaki.lab2_spring.repository;

import com.banayaki.lab2_spring.models.ComputedLayer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ComputedLayerRepository extends CrudRepository<ComputedLayer, Integer> {
    List<ComputedLayer> findByHyperspecter_Id(Integer id);
}
