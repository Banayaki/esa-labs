package com.banayaki.lab2_spring.repository;

import com.banayaki.lab2_spring.models.WatcherEvent;
import org.springframework.data.repository.CrudRepository;

public interface WatcherEventRepository extends CrudRepository<WatcherEvent, Integer> {
}