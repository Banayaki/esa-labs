package com.banayaki.lab2_spring.controller;

import com.banayaki.lab2_spring.models.Email;
import com.banayaki.lab2_spring.models.WatcherEvent;
import com.banayaki.lab2_spring.services.EmailService;
import com.banayaki.lab2_spring.services.WatcherEventService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/utils")
public class UtilsController {

    @Autowired
    EmailService emailService;

    @Autowired
    WatcherEventService watcherEventService;

    @GetMapping("/emails")
    public ResponseEntity<Object> getEmail() throws JsonProcessingException {
        List<Email> emails = emailService.findAll();
        return ResponseEntity.ok().body(new ObjectMapper().writeValueAsString(emails));
    }

    @GetMapping("/events")
    public ResponseEntity<Object> getEvents() throws JsonProcessingException {
        List<WatcherEvent> events = watcherEventService.findAll();
        return ResponseEntity.ok().body(new ObjectMapper().writeValueAsString(events));
    }

}
