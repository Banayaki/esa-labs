package com.banayaki.lab2_spring.controller;

import com.banayaki.lab2_spring.models.Hyperspecter;
import com.banayaki.lab2_spring.models.User;
import com.banayaki.lab2_spring.services.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET, headers = "accept=application/json")
    public List<User> getUsers() {
        List<User> users = userService.findAll();
        return users;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET, headers = "accept=application/xml")
    public ModelAndView getUsersXslt() throws JsonProcessingException {
        List<User> users = userService.findAll();
        ModelAndView modelAndView = new ModelAndView("user");
        Source source = new StreamSource(new ByteArrayInputStream(new XmlMapper().writeValueAsBytes(users)));
        modelAndView.addObject(source);
        return modelAndView;
    }

    @GetMapping("/{userId}")
    public ResponseEntity<Object> getUser(@PathVariable("userId") Integer userId) {
        Optional<User> user = userService.findById(userId);
        if (user.isPresent()) {
            return ResponseEntity.ok().body(user.get());
        } else {
            return new ResponseEntity<Object>(String.format("User with id %s not found", userId), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/")
    public ResponseEntity<Object> addNewUser(@RequestBody User user) {
        userService.save(user);
        return ResponseEntity.ok("New user successfully added");
    }

    @PutMapping("/{userId}")
    public ResponseEntity<Object> updateUser(
            @PathVariable("userId") Integer userId,
            @RequestBody User updatedUser) {
        Optional<User> optionalUser = userService.findById(userId);
        if (!optionalUser.isPresent()) {
            new ResponseEntity<Object>(String.format("User with id %s not found", userId), HttpStatus.NOT_FOUND);
        }

        User user = optionalUser.get();

        if (!updatedUser.getUsername().isEmpty())
            user.setUsername(updatedUser.getUsername());
        if (!updatedUser.getPassword().isEmpty())
            user.setPassword(updatedUser.getPassword());
        if (!updatedUser.getRole().isEmpty())
            user.setRole(updatedUser.getRole());
        userService.save(user);
        return ResponseEntity.ok("User successfully updated");
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<Object> deleteUser(@PathVariable("userId") Integer userId) {
        Optional<User> user = userService.findById(userId);
        if (user.isPresent()) {
            userService.delete(user.get());
            return ResponseEntity.ok("User successfully deleted");
        } else {
            return new ResponseEntity<Object>(String.format("User with id %s not found", userId), HttpStatus.NOT_FOUND);
        }
    }
}
