package com.banayaki.lab2_spring.controller;

import com.banayaki.lab2_spring.models.ComputedLayer;
import com.banayaki.lab2_spring.models.INDILayer;
import com.banayaki.lab2_spring.services.ComputedLayerService;
import com.banayaki.lab2_spring.services.INDILayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/layers")
public class LayersController {

    @Autowired
    ComputedLayerService computedLayerService;

    @Autowired
    INDILayerService indiLayerService;

    @GetMapping("/computed/{specterId}")
    public List<ComputedLayer> getComputedLayes(@PathVariable("specterId") Integer specterId) {
        List<ComputedLayer> computedLayerList = computedLayerService.findByHyperspecterId(specterId);
        return computedLayerList;
    }

    @GetMapping("/indi/{specterId}")
    public List<INDILayer> getIndiLayers(@PathVariable("specterId") Integer specterId) {
        List<INDILayer> indiLayers = indiLayerService.findByHyperspecterId(specterId);
        return indiLayers;
    }
}
