package com.banayaki.lab2_spring.controller;

import com.banayaki.lab2_spring.models.Email;
import com.banayaki.lab2_spring.models.Hyperspecter;
import com.banayaki.lab2_spring.models.OrthophotoMeta;
import com.banayaki.lab2_spring.models.User;
import com.banayaki.lab2_spring.services.HyperspecterService;
import com.banayaki.lab2_spring.services.OrthophotoMetaService;
import com.banayaki.lab2_spring.services.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/specters")
public class HyperspecterController {
    private final Logger logger = LoggerFactory.getLogger(HyperspecterController.class);
    String jsonType = "application/json";
    String xmlType = "application/xml";
    @Autowired
    private HyperspecterService hyperspecterService;
    @Autowired
    private UserService userService;
    @Autowired
    private OrthophotoMetaService orthophotoMetaService;

    @RequestMapping(value = "/", method = RequestMethod.GET, headers = "accept=application/json")
    public ResponseEntity<Object> getSpecters() throws JsonProcessingException {
        /*
          BN: If there is no accept header provided by the user, this method will be invoked by default
         */
        List<Hyperspecter> specters = hyperspecterService.findAll();
        return ResponseEntity.ok().body(new ObjectMapper().writeValueAsString(specters));
    }

    @RequestMapping(value = "/", method = RequestMethod.GET, headers = "accept=application/xml")
    public ModelAndView getXsltView() throws JsonProcessingException {
        List<Hyperspecter> specters = hyperspecterService.findAll();
        ModelAndView modelAndView = new ModelAndView("hyperspecter");
        Source source = new StreamSource(new ByteArrayInputStream(new XmlMapper().writeValueAsBytes(specters)));
        modelAndView.addObject(source);
        return modelAndView;
    }

    @RequestMapping(value = "/{specterId}", method = RequestMethod.GET)
    public ResponseEntity<Object> getSpecter(@PathVariable("specterId") Integer specterId) {
        Optional<Hyperspecter> specter = hyperspecterService.findById(specterId);
        if (specter.isPresent()) {
            return ResponseEntity.ok().body(specter.get());
        } else {
            return new ResponseEntity<Object>(String.format("Hyperspecter with id %s not found", specterId), HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Object> addNewHyperspecter(@RequestParam String username, @RequestBody Hyperspecter hyperspecter) {
        User user = userService.findByUsername(username).get();
        hyperspecter.setUser(user);

        hyperspecter.setPath("/mnt/" + System.currentTimeMillis());
        hyperspecterService.save(hyperspecter);
        return ResponseEntity.ok("Successfully added");
    }

    @RequestMapping(value = "/{specterId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateSpecter(
            @PathVariable("specterId") Integer specterId,
            @RequestParam(defaultValue = "") String username,
            @RequestBody Hyperspecter hyperspecter
    ) {
        Optional<Hyperspecter> optionalHyperspecter = hyperspecterService.findById(specterId);
        if (!optionalHyperspecter.isPresent()) {
            return new ResponseEntity<Object>(String.format("Hyperspecter with id %s not found", specterId), HttpStatus.NOT_FOUND);
        }

        Hyperspecter targetSpecter = optionalHyperspecter.get();

        if (!username.isEmpty()) {
            Optional<User> user = userService.findByUsername(username);
            if (user.isPresent()) {
                targetSpecter.setUser(user.get());
            } else {
                return new ResponseEntity<Object>(String.format("User with username %s not found", username), HttpStatus.NOT_FOUND);
            }
        }

        if (!hyperspecter.getName().isEmpty())
            targetSpecter.setName(hyperspecter.getName());
        if (!hyperspecter.getDescription().isEmpty())
            targetSpecter.setDescription(hyperspecter.getDescription());
        if (hyperspecter.getVisible() != targetSpecter.getVisible())
            targetSpecter.setVisible(hyperspecter.getVisible());
        hyperspecterService.save(targetSpecter);
        return ResponseEntity.ok("Successfully updated");
    }

    @RequestMapping(value = "/{specterId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteSpecter(@PathVariable("specterId") Integer specterId) {
        Optional<Hyperspecter> specter = hyperspecterService.findById(specterId);
        if (specter.isPresent()) {
            hyperspecterService.delete(specter.get());
            return ResponseEntity.ok("Successfully deleted");
        } else {
            return new ResponseEntity<Object>(String.format("Hyperspecter with id %s not found", specterId), HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/orthophoto/meta/{specterId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> getSpecterOrthophotoMeta(@PathVariable("specterId") Integer specterId) {
        Optional<OrthophotoMeta> optionalMeta = orthophotoMetaService.findByHyperspecterId(specterId);
        if (optionalMeta.isPresent()) {
            OrthophotoMeta meta = optionalMeta.get();
            return ResponseEntity.ok().body(meta);
        } else {
            return new ResponseEntity<Object>(String.format("Hyperspecter with id %s has no any orthophotoMeta information." +
                    " Probably this is a default specter?", specterId), HttpStatus.NOT_FOUND);
        }
    }
}
