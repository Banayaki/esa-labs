package com.banayaki.lab2_spring.jms;

import com.banayaki.lab2_spring.models.Email;
import com.banayaki.lab2_spring.models.WatcherEvent;
import com.banayaki.lab2_spring.services.EmailService;
import com.banayaki.lab2_spring.services.WatcherEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class JmsMessageReceiver {

    @Autowired
    EmailService emailService;

    @Autowired
    WatcherEventService watcherEventService;

    @JmsListener(destination = "mail")
    public void receiveEmail(Email email) {
        this.emailService.save(email);
        System.out.println("Received <" + email + ">");
    }

    @JmsListener(destination = "event")
    public void receiveWatcherEvent(WatcherEvent event) {
        this.watcherEventService.save(event);
        System.out.println("Received <" + event + ">");
    }
}
