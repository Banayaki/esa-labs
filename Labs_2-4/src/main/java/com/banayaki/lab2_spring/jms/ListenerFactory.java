package com.banayaki.lab2_spring.jms;

public interface ListenerFactory {
    EventListener createEmailListener();
    EventListener createEventLoggerListener();
}
