package com.banayaki.lab2_spring.jms;

import com.banayaki.lab2_spring.models.WatcherEvent;
import org.springframework.jms.core.JmsTemplate;

public class EventLoggerListener implements EventListener {

    JmsTemplate jmsTemplate;

    public EventLoggerListener(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void update(String eventType, WatcherEvent event) {
        jmsTemplate.convertAndSend("event", event);
    }
}
