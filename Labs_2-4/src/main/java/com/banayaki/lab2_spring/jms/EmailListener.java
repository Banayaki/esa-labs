package com.banayaki.lab2_spring.jms;

import com.banayaki.lab2_spring.models.Email;
import com.banayaki.lab2_spring.models.WatcherEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class EmailListener implements EventListener {

    JmsTemplate jmsTemplate;

    public EmailListener(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void update(String eventType, WatcherEvent event) {
        String msg = String.format("%s happend on object of type %s.", eventType, event.getEntity());
        jmsTemplate.convertAndSend("mail", new Email("admin", msg));
    }
}
