package com.banayaki.lab2_spring.jms;

import com.banayaki.lab2_spring.models.WatcherEvent;

public interface EventListener {
    void update(String eventType, WatcherEvent event);
}
