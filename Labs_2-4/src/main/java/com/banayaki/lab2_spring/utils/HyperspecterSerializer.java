package com.banayaki.lab2_spring.utils;

import com.banayaki.lab2_spring.models.Hyperspecter;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class HyperspecterSerializer extends StdSerializer<Hyperspecter> {
    /**
     * This class is using to serialize hyperspecter object in respect to hide user details (write only username) and
     * hide path where hyperspecter persisted
     */

    public HyperspecterSerializer() {
        this(null);
    }

    public HyperspecterSerializer(Class<Hyperspecter> t) {
        super(t);
    }

    @Override
    public void serialize(Hyperspecter hyperspecter, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", hyperspecter.getId());
        jsonGenerator.writeStringField("name", hyperspecter.getName());
        jsonGenerator.writeStringField("type", hyperspecter.getType());
        jsonGenerator.writeStringField("description", hyperspecter.getDescription());
        jsonGenerator.writeNumberField("height", hyperspecter.getHeight());
        jsonGenerator.writeNumberField("width", hyperspecter.getWidth());
        jsonGenerator.writeNumberField("layersCount", hyperspecter.getLayersCount());
        jsonGenerator.writeNumberField("r", hyperspecter.getR());
        jsonGenerator.writeNumberField("g", hyperspecter.getG());
        jsonGenerator.writeNumberField("b", hyperspecter.getB());
        jsonGenerator.writeStringField("username", hyperspecter.getUser().getUsername());
        jsonGenerator.writeBooleanField("visible", hyperspecter.getVisible());
        jsonGenerator.writeEndObject();
    }
}
