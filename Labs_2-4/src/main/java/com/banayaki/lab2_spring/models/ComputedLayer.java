package com.banayaki.lab2_spring.models;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "computedLayer")
public class ComputedLayer implements Serializable {
    /**
     * Stores names of computed layers like PCA, 'Kmeans N', etc.
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specterId")
    private Hyperspecter hyperspecter;

    @Column(name = "name", nullable = false)
    private String name;

    public Hyperspecter getHyperspecter() {
        return hyperspecter;
    }
}
