package com.banayaki.lab2_spring.models;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "orthophotoMeta")
@Data
public class OrthophotoMeta implements Serializable {
    /**
     * Linking hyperspecter (of type Orthophoto) with ODM service
     * It's used to get tiles for leaflet.js rendering
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ToString.Exclude
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specterId")
    private Hyperspecter hyperspecter;

    @Column(name = "projectId", nullable = false)
    private String projectId;

    @Column(name = "taskId", nullable = false)
    private String taskId;
}
