package com.banayaki.lab2_spring.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "sql_watcher")
public class WatcherEvent {

    public WatcherEvent(String action, String entity, String substance) {
        this.action = action;
        this.entity = entity;
        this.substance = substance;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column
    private String action;

    @Column
    private String entity;

    @Column
    private String substance;
}
