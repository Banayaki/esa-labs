package com.banayaki.lab2_spring.models;

public final class EventType {
    public static final String INSERT_TYPE = "insert";
    public static final String UPDATE_TYPE = "update";
    public static final String DELETE_TYPE = "delete";
    public static final String SELECT_TYPE = "select";
}
