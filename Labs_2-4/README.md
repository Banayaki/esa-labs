# Assignment №2

# Application using Spring Framework

## Task 1

I'm using the same subject areas as I used in the previous assignment.

## Task 2

Models have several differences between previous and current assignments:

* Annotations `@NamedQueries` were deleted
* Classes do not implement `Serializable` interface

The database is managed by the `CrudRepository`. 
For each entity class was created its own *CrudRespository* interface.

## Task 3

The business logic is quite similar to the logic in the previous assignment. But in this time we
did not use the named quires since `CrudRepository` already provides desired methods. Annotations
`@Transactional` and `@Repository` help to manage transactions and database access:d. 

Spring helps to build both data and business layers **much easier** than JavaEE!

## Task 4

Controllers have some minor changes, which were changed JavaEE annotations on Spring equivalents.

## Conclusions

* Fast development 
* Less boilerplate code
* Data layers much easier

# Assignment №3

# RESTful web-service

## Task 1-3

The API is already RESTful!

## Task 4-5

To be able to return the XML object the user should provide an `accept` header with value: `application/xml`, otherwise the json would be returned.

For some endpoints (`/specters`, and `/users`) XSLT response is available. 

![xslt](images/xslt.png)

# Assignment №4

## Task 1

The following table was created:

![new table](images/sql_watcher.png)

## Task 2

To add a new logging system I applied Spring AOP.

I created a new class - `LoggingAspect` which watches for services methods and logs all the events into the JMS using the `convertAndSend` method.

## Task 3

A new model for the sql_watcher table was created. For that model was coded repository and service classes. Also, to prevent recursive logging via AOP, I've created the annotation `@NoLoggin` and applied it to methods of sql_watcher service. 

*Using the AOP makes me able to add a logging system without modification of the existing classes and logic.*

## Task 4

In `JmsMessageReceiver` I add a listener which listens for the "events" and adds each event into the sql_watcher table.

## Task 5

The following table was created:

![new table](images/emails.png)

## Task 6

Using the same `LoggingAspect` I added an email notification when a user is trying to access nonexisting data, for example:

Let specter with `id=1` is present in the database. Then the user is trying to get a specter with `id=2` *(GET /specters/2)*. The `LoggingAspect` detects this behavior and makes an email notification.