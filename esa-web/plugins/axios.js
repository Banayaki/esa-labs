export default function ({ $axios }) {
    if (process.client) {
        const host = window.location.hostname
        const protocol = window.location.protocol
        const port = 8080
        $axios.setBaseURL(protocol + '//' + host + ':' + port)
    }
}