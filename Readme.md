# Mukhin Artem 6133-010402D

# Assignments status

* Assignment #1  :heavy_check_mark:
* Assignment #2  :heavy_check_mark:
* Assignment #3  :heavy_check_mark:
* Assignment #4  :heavy_check_mark:

## Assignment #1

The code is located in the `Lab1_JavaEE` folder.

## Assignment #2-4

The code is located in the `Labs_2-4` folder. All the assigmnets (2-4) were made using `Spring Boot`.

## Web Applocation O_o

In the folder `esa-web` you can find a web-application which may be used for some testing. 
But i recommend using `Postman` app.

![](/images/web-app.png)