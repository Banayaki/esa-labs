# Assignment №1

# Application with common JavaEE architecture

## Made by Artem Mukhin. 6133-010402D.

# Task 1

## Setting up an application server

We downloaded glassfish 5 and put it in the root of the filesystem.

The application server was started using the following command:

`bash asadmin start-domain --verbose`

## Setting up connection to a database

Glassfish cannot set up the connection to the database without a proper JDBC driver. Therefore, we manually put the JDBC
driver of PostgreSQL DBMS into the glassfish folder.

### Create a JDBC Connection Pool

![Create JDBC Connection Pool 1](images/CreateJDBCConnectionPool_1.png)
![Create JDBC Connection Pool 2](images/CreateJDBCConnectionPool_2.png)

### Create a JDBC Resource

![Create a JDBC Resource.png](images/CreateaJDBCResource.png)

# Task 2

To launch the **PostgreSQL** and Adminer we use docker with the following command:

`docker-compose up --build`

# Task 3

## Logical structure

![Database structure](images/DatabaseStructure.png)

## Tables description

<table>
    <thead>
        <td>Table</td>
        <td>Description</td>
    </thead>
    <tr>
        <td>Client</td>
        <td>Stores information about users, their password, and role.</td>
    </tr>
    <tr>
        <td>Hyperspecter</td>
        <td>Stores information about hyperspecters, their meta data</td>
    </tr>
    <tr>
        <td>Computed Layer</td>
        <td>Stores information about computed layers like results of PCA or KMEANS processing</td>
    </tr>
    <tr>
        <td>Indi Layer</td>
        <td>Stores information about computed Informative Indices</td>
    </tr>
    <tr>
        <td>Orthphoto meta</td>
        <td>Stores information about hyperspecter's orthophoto meta</td>
    </tr>
</table>

## Attribution description

### Let's imagine here a table with tables attributes description

# Task 4

For each table, we've created our own Java Bean using **Loombok library** to remove boilerplate code. Also, we provided
classes with `NamedQueries` annotation which defines some queries for accessing the database. We've created these
queries due to JPA's unclear behavior with relations One2Many, Many2One, Many2Many. It works properly only one way.

# Task 5

We implemented the Data Access Object pattern to provide access to the database. The following scheme depicts our
application backend.
![Backend](images/Scturcture.png)

# Task 6

As a view layer, we implemented several web servlets which provide REST API for Hyperpsecter and User tables. In the
servlets, dao objects are injected using dependency injection annotation (@Inject)
