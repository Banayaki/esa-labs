import mukhin_parenski.lab1_javaee.models.*;
import mukhin_parenski.lab1_javaee.repository.*;
import mukhin_parenski.lab1_javaee.repository.interfaces.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class TestDao {
    public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("test");

    @BeforeAll
    public static void beforeTests() {
        UserDao userDao = new UserDaoImpl(emf.createEntityManager());
        User defaultUser = new User();
        defaultUser.setUsername("default user");
        defaultUser.setPassword("superhashedpassword");
        userDao.save(defaultUser);
    }

    @Test
    public void testCRUDSpecter() {
        HyperspecterDao hyperspecterDao = new HyperspecterDaoImpl(emf.createEntityManager());
        UserDao userDao = new UserDaoImpl(emf.createEntityManager());
        User defaultUser = userDao.get(1).get();

        Hyperspecter test1 = new Hyperspecter();
        test1.setName("test1");
        test1.setPath("/mnt/data/1");
        test1.setDescription("Test specter 1");
        test1.setHeight(1080);
        test1.setWidth(1920);
        test1.setLayersCount(120);
        test1.setR(30);
        test1.setG(40);
        test1.setB(50);
        test1.setUser(defaultUser);

        hyperspecterDao.save(test1);

        assertEquals(hyperspecterDao.getAll().size(), 1);
        assertEquals(hyperspecterDao.get(test1.getId()).get(), test1);

        hyperspecterDao.deleteById(test1.getId());
        assertFalse(hyperspecterDao.get(test1.getId()).isPresent());

        test1.setId(null);
        hyperspecterDao.save(test1);
        hyperspecterDao.delete(test1);
        assertFalse(hyperspecterDao.get(test1.getId()).isPresent());

        test1.setId(null);
        String newName = "NEW NAME";
        hyperspecterDao.save(test1);
        test1.setName(newName);
        hyperspecterDao.update(test1);
        assertEquals(hyperspecterDao.get(test1.getId()).get().getName(), newName);
    }

    @Test
    public void testCRUDSpecterWithLayers() {
        EntityManager em = emf.createEntityManager();
        HyperspecterDao hyperspecterDao = new HyperspecterDaoImpl(em);
        UserDao userDao = new UserDaoImpl(em);
        ComputedLayerDao computedLayerDao = new ComputedLayerDaoImpl(em);
        INDILayerDao indiLayerDao = new INDILayerDaoImpl(em);
        OrthophotoMetaDao orthophotoMetaDao = new OrthophotoMetaDaoImpl(em);

        User defaultUser = userDao.get(1).get();
        ComputedLayer computedLayer = new ComputedLayer();
        computedLayer.setName("PCA");

        INDILayer indiLayer = new INDILayer();
        indiLayer.setName("INDI 23 49");
        indiLayer.setL1(23);
        indiLayer.setL2(49);
        indiLayer.setThresholds("128,230,90,130");

        OrthophotoMeta orthophotoMeta = new OrthophotoMeta();
        orthophotoMeta.setProjectId("3123asd-asdf34");
        orthophotoMeta.setTaskId("asdf-03213");

        Hyperspecter test1 = new Hyperspecter();
        test1.setName("test1");
        test1.setPath("/mnt/data/1");
        test1.setDescription("Test specter 1");
        test1.setHeight(1080);
        test1.setWidth(1920);
        test1.setLayersCount(120);
        test1.setR(30);
        test1.setG(40);
        test1.setB(50);
        test1.setUser(defaultUser);
        hyperspecterDao.save(test1);

        assertEquals(hyperspecterDao.getAll().size(), 1);

        test1.addComputedLayer(computedLayer);
        test1.addIndiLayer(indiLayer);
        test1.setOrthophotoMeta(orthophotoMeta);
        hyperspecterDao.update(test1);

        System.out.println("TEST " + hyperspecterDao.get(1).get());
        assertEquals(hyperspecterDao.get(1).get().getIndiLayers().size(), 1);

        hyperspecterDao.deleteById(test1.getId());
        assertFalse(hyperspecterDao.get(test1.getId()).isPresent());
        assertNull(computedLayer.getId());

        test1.setComputedLayers(new HashSet<>());
        test1.setIndiLayers(new HashSet<>());
        test1.setOrthophotoMeta(null);
        test1.setId(null);
        hyperspecterDao.save(test1);
        hyperspecterDao.delete(test1);
        assertFalse(hyperspecterDao.get(test1.getId()).isPresent());

        test1.setId(null);
        String newName = "NEW NAME";
        hyperspecterDao.save(test1);
        test1.setName(newName);
        hyperspecterDao.update(test1);
        assertEquals(hyperspecterDao.get(test1.getId()).get().getName(), newName);
    }
}
