package mukhin_parenski.lab1_javaee.models;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "indiLayer")
@Data
@NamedQueries({
        @NamedQuery(name = "INDILayer.getAll", query = "select l from INDILayer l"),
        @NamedQuery(name = "INDILayer.getBySpecterId", query = "select l from INDILayer l left join fetch l.hyperspecter h where h.id = :id")
})
public class INDILayer implements Serializable {
    /**
     * INDI stands for Informative Normalized Difference Index
     * l1, l2 - layers id used to compute informative index
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specterId")
    private Hyperspecter hyperspecter;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "l1", nullable = false)
    private Integer l1;

    @Column(name = "l2", nullable = false)
    private Integer l2;

    @Column(name = "description")
    private String description;

    @Column(name = "thresholds", nullable = false)
    private String thresholds;

    @Column(name = "objectPreviewFilename")
    private String objectPreviewFilename;

    @Column(name = "indexPreviewFilename")
    private String indexPreviewFilename;

    @Column(name = "resultPreviewFilename")
    private String resultPreviewFilename;
}
