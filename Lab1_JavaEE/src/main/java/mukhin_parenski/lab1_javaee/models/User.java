package mukhin_parenski.lab1_javaee.models;


import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "client")
@Data
@NamedQueries({
        @NamedQuery(name = "User.getAll", query = "select u from User u"),
        @NamedQuery(name = "User.getByUsername", query = "select u from User u where u.username = :username")
})
public class User implements Serializable {
    public static String DEFAULT_USER = "user";
    public static String ADMIN_USER = "admin";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "username")
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "role", nullable = false)
    private String role = User.ADMIN_USER;
}
