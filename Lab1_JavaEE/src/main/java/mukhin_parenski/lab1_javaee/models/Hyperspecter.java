package mukhin_parenski.lab1_javaee.models;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "hyperspecter")
@Data
@NamedQueries({
        @NamedQuery(name = "Hyperspecter.getAll", query = "select distinct h from Hyperspecter h" +
                " left join fetch h.user left join fetch h.computedLayers left join fetch h.indiLayers left join fetch h.orthophotoMeta"),
        @NamedQuery(name = "Hyperspecter.getById", query = "select h from Hyperspecter h" +
                " left join fetch h.user left join fetch h.computedLayers left join fetch h.indiLayers left join fetch h.orthophotoMeta" +
                " where h.id = :id"),
        @NamedQuery(name = "Hyperspecter.getByUsername", query = "select h from Hyperspecter h" +
                " left join fetch h.user u left join fetch h.computedLayers left join fetch h.indiLayers left join fetch h.orthophotoMeta" +
                " where u.username = :username"),
        @NamedQuery(name = "Hyperspecter.deleteById", query = "delete from Hyperspecter h where h.id = :id"),
})
public class Hyperspecter implements Serializable {
    /**
     * Represents Hyperspecter object related to HSI. Contains all necessary for work properties
     * <p>
     * Data annotation stands for reducing boilerplate code (via "Structure" btn u can see that all setters/getters
     * etc. were created)
     */
    public static String TYPE_ORTHOPHOTO = "orthophoto";
    public static String TYPE_SPECTER = "hyperspecter";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type = Hyperspecter.TYPE_SPECTER;

    @Column(name = "path", nullable = false, unique = true)
    private String path;

    @Column(name = "description")
    private String description;

    @Column(name = "height", nullable = false)
    private Integer height;

    @Column(name = "width", nullable = false)
    private Integer width;

    @Column(name = "layersCount", nullable = false)
    private Integer layersCount;

    @Column(name = "r", nullable = false)
    private Integer r;

    @Column(name = "g", nullable = false)
    private Integer g;

    @Column(name = "b", nullable = false)
    private Integer b;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", nullable = false)
    private User user;

    @Column(name = "visible")
    private Boolean visible = true;

    @OneToMany(mappedBy = "hyperspecter", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<ComputedLayer> computedLayers = new HashSet<>();

    @OneToMany(mappedBy = "hyperspecter", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<INDILayer> indiLayers = new HashSet<>();

    @OneToOne(mappedBy = "hyperspecter", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private OrthophotoMeta orthophotoMeta;

    public void addComputedLayer(ComputedLayer computedLayer) {
        computedLayers.add(computedLayer);
        computedLayer.setHyperspecter(this);
    }

    public void addIndiLayer(INDILayer indiLayer) {
        indiLayers.add(indiLayer);
        indiLayer.setHyperspecter(this);
    }
}
