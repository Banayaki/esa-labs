package mukhin_parenski.lab1_javaee.models;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "computedLayer")
@Data
@NamedQueries({
        @NamedQuery(name = "ComputedLayer.getAll", query = "select l from ComputedLayer l"),
        @NamedQuery(name = "ComputedLayer.getBySpecterId", query = "select l from ComputedLayer l left join fetch l.hyperspecter h where h.id = :id")
})
public class ComputedLayer implements Serializable {
    /**
     * Stores names of computed layers like PCA, 'Kmeans N', etc.
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specterId")
    private Hyperspecter hyperspecter;

    @Column(name = "name", nullable = false)
    private String name;
}
