package mukhin_parenski.lab1_javaee.models;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "orthophotoMeta")
@Data
@NamedQueries({
        @NamedQuery(name = "OrthophotoMeta.getAll", query = "select l from OrthophotoMeta l"),
        @NamedQuery(name = "OrthophotoMeta.getBySpecterId", query = "select m from OrthophotoMeta m left join fetch m.hyperspecter h where h.id = :id")
})
public class OrthophotoMeta implements Serializable {
    /**
     * Linking hyperspecter (of type Orthophoto) with ODM service
     * It's used to get tiles for leaflet.js rendering
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ToString.Exclude
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specterId")
    private Hyperspecter hyperspecter;

    @Column(name = "projectId", nullable = false)
    private String projectId;

    @Column(name = "taskId", nullable = false)
    private String taskId;
}
