package mukhin_parenski.lab1_javaee.repository;

import mukhin_parenski.lab1_javaee.models.Hyperspecter;
import mukhin_parenski.lab1_javaee.repository.interfaces.HyperspecterDao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Stateless
public class HyperspecterDaoImpl extends TransactionHandler implements HyperspecterDao {

    @PersistenceContext(unitName = "default")
    private EntityManager em;

    public HyperspecterDaoImpl() {
    }

    public HyperspecterDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Optional<Hyperspecter> get(Integer id) {
        TypedQuery<Hyperspecter> query = em.createNamedQuery("Hyperspecter.getById", Hyperspecter.class)
                .setParameter("id", id);
        try {
            Hyperspecter specter = query.getSingleResult();
            return Optional.ofNullable(specter);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Hyperspecter> getAll() {
        TypedQuery<Hyperspecter> query = em.createNamedQuery("Hyperspecter.getAll", Hyperspecter.class);
        return query.getResultList();
    }

    @Override
    public void save(Hyperspecter hyperspecter) {
        em.persist(hyperspecter);
    }

    @Override
    public void update(Hyperspecter hyperspecter) {
        em.merge(hyperspecter);
    }

    @Override
    public void delete(Hyperspecter hyperspecter) {
        em.remove(em.contains(hyperspecter) ? hyperspecter : em.merge(hyperspecter));
    }

    @Override
    public void deleteById(Integer id) {
        Hyperspecter hyperspecter = this.get(id).get();
        this.delete(hyperspecter);
    }

    @Override
    public Hyperspecter getById(Integer id) {
        TypedQuery<Hyperspecter> query = em.createNamedQuery("Hyperspecter.getById", Hyperspecter.class)
                .setParameter("id", id);
        return query.getSingleResult();
    }

    @Override
    public List<Hyperspecter> getByUsername(String username) {
        TypedQuery<Hyperspecter> query = em.createNamedQuery("Hyperspecter.getByUsername", Hyperspecter.class)
                .setParameter("username", username);
        List<Hyperspecter> results = query.getResultList();
        return results;
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
