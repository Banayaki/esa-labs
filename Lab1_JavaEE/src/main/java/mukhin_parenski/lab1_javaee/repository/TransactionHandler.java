package mukhin_parenski.lab1_javaee.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.function.Consumer;

public abstract class TransactionHandler {

    protected void executeInsideTransaction(Consumer<EntityManager> action) {
        /**
         * Decorator for reducing boilerplate code
         */
        EntityTransaction tx = getEntityManager().getTransaction();
        try {
            tx.begin();
            action.accept(getEntityManager());
            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        }
    }

    protected abstract EntityManager getEntityManager();
}
