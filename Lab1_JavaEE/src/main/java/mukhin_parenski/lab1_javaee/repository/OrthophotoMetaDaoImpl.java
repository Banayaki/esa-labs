package mukhin_parenski.lab1_javaee.repository;

import mukhin_parenski.lab1_javaee.models.OrthophotoMeta;
import mukhin_parenski.lab1_javaee.repository.interfaces.OrthophotoMetaDao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Stateless
public class OrthophotoMetaDaoImpl extends TransactionHandler implements OrthophotoMetaDao {

    @PersistenceContext(unitName = "default")
    private EntityManager em;

    public OrthophotoMetaDaoImpl() {
    }

    public OrthophotoMetaDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Optional<OrthophotoMeta> get(Integer id) {
        return Optional.ofNullable(em.find(OrthophotoMeta.class, id));
    }

    @Override
    public List<OrthophotoMeta> getAll() {
        TypedQuery<OrthophotoMeta> query = em.createNamedQuery("OrthophotoMeta.getAll", OrthophotoMeta.class);
        List<OrthophotoMeta> results = query.getResultList();
        return results;
    }

    @Override
    public void save(OrthophotoMeta orthophotoMeta) {
        em.persist(orthophotoMeta);
    }

    @Override
    public void update(OrthophotoMeta orthophotoMeta) {
        em.merge(orthophotoMeta);
    }

    @Override
    public void delete(OrthophotoMeta orthophotoMeta) {
        em.remove(em.contains(orthophotoMeta) ? orthophotoMeta : em.merge(orthophotoMeta));
    }

    @Override
    public Optional<OrthophotoMeta> getBySpecterId(Integer id) {
        TypedQuery<OrthophotoMeta> query = em.createNamedQuery("OrthophotoMeta.getBySpecterId", OrthophotoMeta.class)
                .setParameter("id", id);
        OrthophotoMeta result;
        try {
            result = query.getSingleResult();
        } catch (NoResultException e) {
            result = null;
        }
        return Optional.ofNullable(result);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
