package mukhin_parenski.lab1_javaee.repository;

import mukhin_parenski.lab1_javaee.models.INDILayer;
import mukhin_parenski.lab1_javaee.repository.interfaces.INDILayerDao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Stateless
public class INDILayerDaoImpl extends TransactionHandler implements INDILayerDao {

    @PersistenceContext(unitName = "default")
    private EntityManager em;

    public INDILayerDaoImpl() {
    }

    public INDILayerDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Optional<INDILayer> get(Integer id) {
        return Optional.ofNullable(em.find(INDILayer.class, id));
    }

    @Override
    public List<INDILayer> getAll() {
        TypedQuery<INDILayer> query = em.createNamedQuery("INDILayer.getAll", INDILayer.class);
        List<INDILayer> results = query.getResultList();
        return results;
    }

    @Override
    public void save(INDILayer indiLayer) {
        em.persist(indiLayer);
    }

    @Override
    public void update(INDILayer indiLayer) {
        em.merge(indiLayer);
    }

    @Override
    public void delete(INDILayer indiLayer) {
        em.remove(em.contains(indiLayer) ? indiLayer : em.merge(indiLayer));
    }

    @Override
    public List<INDILayer> getBySpecterId(Integer id) {
        TypedQuery<INDILayer> query = em.createNamedQuery("INDILayer.getBySpecterId", INDILayer.class)
                .setParameter("id", id);
        return query.getResultList();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
