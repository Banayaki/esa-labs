package mukhin_parenski.lab1_javaee.repository.interfaces;

import mukhin_parenski.lab1_javaee.models.INDILayer;

import java.util.List;

public interface INDILayerDao extends Dao<INDILayer> {
    List<INDILayer> getBySpecterId(Integer id);

}
