package mukhin_parenski.lab1_javaee.repository.interfaces;

import java.util.List;
import java.util.Optional;

public interface Dao<T> {
    Optional<T> get(Integer id);

    List<T> getAll();

    void save(T t);

    void update(T t);

    void delete(T t);
}
