package mukhin_parenski.lab1_javaee.repository.interfaces;

import mukhin_parenski.lab1_javaee.models.Hyperspecter;

import java.util.List;

public interface HyperspecterDao extends Dao<Hyperspecter> {
    void deleteById(Integer id);

    Hyperspecter getById(Integer id);

    List<Hyperspecter> getByUsername(String username);
}
