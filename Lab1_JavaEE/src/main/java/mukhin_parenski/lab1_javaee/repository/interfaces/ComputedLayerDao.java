package mukhin_parenski.lab1_javaee.repository.interfaces;

import mukhin_parenski.lab1_javaee.models.ComputedLayer;

import java.util.List;

public interface ComputedLayerDao extends Dao<ComputedLayer> {
    List<ComputedLayer> getBySpecterId(Integer id);
}
