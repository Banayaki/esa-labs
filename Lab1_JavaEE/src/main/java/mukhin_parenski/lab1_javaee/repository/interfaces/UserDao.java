package mukhin_parenski.lab1_javaee.repository.interfaces;

import mukhin_parenski.lab1_javaee.models.User;

public interface UserDao extends Dao<User> {
    User getUserByUsername(String username);
}
