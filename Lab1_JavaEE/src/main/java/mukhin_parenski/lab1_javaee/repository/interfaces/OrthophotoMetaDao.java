package mukhin_parenski.lab1_javaee.repository.interfaces;

import mukhin_parenski.lab1_javaee.models.OrthophotoMeta;

import java.util.Optional;

public interface OrthophotoMetaDao extends Dao<OrthophotoMeta> {
    Optional<OrthophotoMeta> getBySpecterId(Integer id);
}
