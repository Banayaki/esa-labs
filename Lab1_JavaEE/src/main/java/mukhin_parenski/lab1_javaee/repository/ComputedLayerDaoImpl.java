package mukhin_parenski.lab1_javaee.repository;

import mukhin_parenski.lab1_javaee.models.ComputedLayer;
import mukhin_parenski.lab1_javaee.repository.interfaces.ComputedLayerDao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Stateless
public class ComputedLayerDaoImpl extends TransactionHandler implements ComputedLayerDao {

    @PersistenceContext(unitName = "default")
    private EntityManager em;

    public ComputedLayerDaoImpl() {
    }

    public ComputedLayerDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Optional<ComputedLayer> get(Integer id) {
        return Optional.ofNullable(em.find(ComputedLayer.class, id));
    }

    @Override
    public List<ComputedLayer> getAll() {
        TypedQuery<ComputedLayer> query = em.createNamedQuery("ComputedLayer.getAll", ComputedLayer.class);
        List<ComputedLayer> results = query.getResultList();
        return results;
    }

    @Override
    public void save(ComputedLayer computedLayer) {
        em.persist(computedLayer);
    }

    @Override
    public void update(ComputedLayer computedLayer) {
        em.merge(computedLayer);
    }

    @Override
    public void delete(ComputedLayer computedLayer) {
        em.remove(em.contains(computedLayer) ? computedLayer : em.merge(computedLayer));
    }

    @Override
    public List<ComputedLayer> getBySpecterId(Integer id) {
        TypedQuery<ComputedLayer> query = em.createNamedQuery("ComputedLayer.getBySpecterId", ComputedLayer.class)
                .setParameter("id", id);
        return query.getResultList();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
