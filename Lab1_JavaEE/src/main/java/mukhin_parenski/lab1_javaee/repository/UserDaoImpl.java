package mukhin_parenski.lab1_javaee.repository;

import mukhin_parenski.lab1_javaee.models.User;
import mukhin_parenski.lab1_javaee.repository.interfaces.UserDao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Stateless
public class UserDaoImpl extends TransactionHandler implements UserDao {

    @PersistenceContext(unitName = "default")
    private EntityManager em;

    public UserDaoImpl() {
    }

    public UserDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Optional<User> get(Integer id) {
        return Optional.ofNullable(em.find(User.class, id));
    }

    @Override
    public List<User> getAll() {
        TypedQuery<User> query = em.createNamedQuery("User.getAll", User.class);
        List<User> results = query.getResultList();
        return results;
    }

    @Override
    public void save(User user) {
        em.persist(user);
    }

    @Override
    public void update(User user) {
        em.merge(user);
    }

    @Override
    public void delete(User user) {
        em.remove(em.contains(user) ? user : em.merge(user));
    }

    @Override
    public User getUserByUsername(String username) {
        TypedQuery<User> query = em.createNamedQuery("User.getByUsername", User.class)
                .setParameter("username", username);
        return query.getSingleResult();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


}
