package mukhin_parenski.lab1_javaee;

import mukhin_parenski.lab1_javaee.api.HyperspecterApi;
import mukhin_parenski.lab1_javaee.api.LayersApi;
import mukhin_parenski.lab1_javaee.api.UserApi;

import javax.ws.rs.ApplicationPath;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/api")
public class MainApplication extends javax.ws.rs.core.Application {
    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet<Class<?>>();
        classes.add(UserApi.class);
        classes.add(HyperspecterApi.class);
        classes.add(LayersApi.class);
        return classes;
    }
}