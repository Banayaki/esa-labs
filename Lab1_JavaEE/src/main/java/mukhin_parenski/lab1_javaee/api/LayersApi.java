package mukhin_parenski.lab1_javaee.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import mukhin_parenski.lab1_javaee.models.ComputedLayer;
import mukhin_parenski.lab1_javaee.models.INDILayer;
import mukhin_parenski.lab1_javaee.repository.interfaces.ComputedLayerDao;
import mukhin_parenski.lab1_javaee.repository.interfaces.INDILayerDao;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@WebServlet
@Path("/layers")
public class LayersApi {

    @EJB
    ComputedLayerDao computedLayerDao;

    @EJB
    INDILayerDao indiLayerDao;

    @GET
    @Path("/computed/{specterId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getComputedLayers(@PathParam("specterId") String specterId) throws JsonProcessingException {
        List<ComputedLayer> layers = computedLayerDao.getBySpecterId(Integer.valueOf(specterId));
        return Response.status(Response.Status.OK.getStatusCode())
                .entity(layers)
                .build();
    }

    @GET
    @Path("/indi/{specterId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIndiLayers(@PathParam("specterId") String specterId) throws JsonProcessingException {
        List<INDILayer> layers = indiLayerDao.getBySpecterId(Integer.valueOf(specterId));
        return Response.status(Response.Status.OK.getStatusCode())
                .entity(layers)
                .build();
    }
}
