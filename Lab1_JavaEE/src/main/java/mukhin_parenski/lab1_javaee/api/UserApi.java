package mukhin_parenski.lab1_javaee.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mukhin_parenski.lab1_javaee.models.User;
import mukhin_parenski.lab1_javaee.repository.interfaces.UserDao;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Path("/users")
public class UserApi {

    @EJB
    private UserDao userDao;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() throws JsonProcessingException {
        List<User> users = userDao.getAll();
        return Response.status(Response.Status.OK.getStatusCode())
                .entity(objectMapper.writeValueAsString(users))
                .build();
    }

    @GET
    @Path("/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("userId") String userId) throws JsonProcessingException {
        Optional<User> user = userDao.get(Integer.valueOf(userId));
        if (user.isPresent()) {
            return Response.status(Response.Status.OK.getStatusCode())
                    .entity(objectMapper.writeValueAsString(user))
                    .build();
        } else {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode())
                    .entity(String.format("User with id %s not found", userId))
                    .build();
        }
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addNewUser(
            @FormParam("username") String username,
            @FormParam("password") String password,
            @FormParam("role") String role) {
        User user = new User();
        user.setUsername(username);
        user.setPassword("hashed_" + password);
        user.setRole(role);
        userDao.save(user);
        return Response.status(Response.Status.OK.getStatusCode()).build();
    }

    @PUT
    @Path("/{userId}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updateUser(
            @PathParam("userId") String userId,
            @DefaultValue("") @FormParam("username") String username,
            @DefaultValue("") @FormParam("password") String password,
            @DefaultValue("") @FormParam("role") String role) {
        Optional<User> optionalUser = userDao.get(Integer.valueOf(userId));
        if (!optionalUser.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode())
                    .entity(String.format("User with id %s not found", userId))
                    .build();
        }

        User user = optionalUser.get();

        if (!username.isEmpty())
            user.setUsername(username);
        if (!password.isEmpty())
            user.setPassword("hashed_" + password);
        if (!role.isEmpty())
            user.setRole(role);
        userDao.update(user);
        return Response.status(Response.Status.OK.getStatusCode()).build();
    }

    @DELETE
    @Path("/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("userId") String userId) {
        Optional<User> user = userDao.get(Integer.valueOf(userId));
        if (user.isPresent()) {
            userDao.delete(user.get());
            return Response.status(Response.Status.OK.getStatusCode()).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode())
                    .entity(String.format("User with id %s not found", userId))
                    .build();
        }
    }
}
