package mukhin_parenski.lab1_javaee.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import mukhin_parenski.lab1_javaee.models.Hyperspecter;
import mukhin_parenski.lab1_javaee.models.OrthophotoMeta;
import mukhin_parenski.lab1_javaee.models.User;
import mukhin_parenski.lab1_javaee.repository.interfaces.HyperspecterDao;
import mukhin_parenski.lab1_javaee.repository.interfaces.OrthophotoMetaDao;
import mukhin_parenski.lab1_javaee.repository.interfaces.UserDao;
import mukhin_parenski.lab1_javaee.utils.HyperspecterSerializer;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.servlet.annotation.WebServlet;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@WebServlet
@Path("/specters")
public class HyperspecterApi {

    @EJB
    private HyperspecterDao hyperspecterDao;
    @EJB
    private UserDao userDao;
    @EJB
    private OrthophotoMetaDao orthophotoMetaDao;

    private static final ObjectMapper objectMapper = new ObjectMapper();

    static {
        HyperspecterSerializer hyperspecterSerializer = new HyperspecterSerializer(Hyperspecter.class);
        SimpleModule module = new SimpleModule("HyperspecterSerializer", new Version(1, 1, 1, null));
        module.addSerializer(Hyperspecter.class, hyperspecterSerializer);
        objectMapper.registerModule(module);
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSpecters() throws JsonProcessingException {
        List<Hyperspecter> specters = hyperspecterDao.getAll();
        return Response.status(Response.Status.OK.getStatusCode())
                .entity(objectMapper.writeValueAsString(specters))
                .build();
    }

    @GET
    @Path("/{specterId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSpecter(@PathParam("specterId") String specterId) throws JsonProcessingException {
        Optional<Hyperspecter> specter = hyperspecterDao.get(Integer.valueOf(specterId));
        if (specter.isPresent()) {
            return Response.status(Response.Status.OK.getStatusCode())
                    .entity(objectMapper.writeValueAsString(specter.get()))
                    .build();
        } else {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode())
                    .entity(String.format("Hyperspecter with id %s not found", specterId))
                    .build();
        }
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addNewHyperspecter(@QueryParam("username") String username, Hyperspecter hyperspecter) {
        User user = userDao.getUserByUsername(username);
        hyperspecter.setUser(user);

        hyperspecter.setPath("/mnt/" + String.valueOf(System.currentTimeMillis()));
        hyperspecterDao.save(hyperspecter);
        return Response.status(Response.Status.OK.getStatusCode()).build();
    }

    @PUT
    @Path("/{specterId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateSpecter(
            @PathParam("specterId") String specterId,
            @DefaultValue("") @QueryParam("username") String username,
            Hyperspecter hyperspecter
    ) {
        Optional<Hyperspecter> optionalHyperspecter = hyperspecterDao.get(Integer.valueOf(specterId));
        if (!optionalHyperspecter.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode())
                    .entity(String.format("User with id %s not found", specterId))
                    .build();
        }

        Hyperspecter targetSpecter = optionalHyperspecter.get();

        if (!username.isEmpty()) {
            try {
                User user = userDao.getUserByUsername(username);
                targetSpecter.setUser(user);
            } catch (NoResultException e) {
                return Response.status(Response.Status.NOT_FOUND.getStatusCode())
                        .entity(String.format("User with username %s not found", username))
                        .build();
            }
        }

        if (!hyperspecter.getName().isEmpty())
            targetSpecter.setName(hyperspecter.getName());
        if (!hyperspecter.getDescription().isEmpty())
            targetSpecter.setDescription(hyperspecter.getDescription());
        if (hyperspecter.getVisible() != targetSpecter.getVisible())
            targetSpecter.setVisible(hyperspecter.getVisible());
        hyperspecterDao.update(targetSpecter);
        return Response.status(Response.Status.OK.getStatusCode()).build();
    }

    @DELETE
    @Path("/{specterId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSpecter(@PathParam("specterId") String specterId) {
        Optional<Hyperspecter> specter = hyperspecterDao.get(Integer.valueOf(specterId));
        if (specter.isPresent()) {
            hyperspecterDao.delete(specter.get());
            return Response.status(Response.Status.OK.getStatusCode()).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode())
                    .entity(String.format("Hyperspecter with id %s not found", specterId))
                    .build();
        }
    }

    @GET
    @Path("/orthophoto/meta/{specterId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSpecterOrthophotoMeta(@PathParam("specterId") String specterId) {
        Optional<OrthophotoMeta> optionalMeta = orthophotoMetaDao.getBySpecterId(Integer.valueOf(specterId));
        if (optionalMeta.isPresent()) {
            OrthophotoMeta meta = optionalMeta.get();
            return Response.status(Response.Status.OK.getStatusCode()).entity(meta).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode())
                    .entity(String.format("Hyperspecter with id %s has no any orthophotoMeta information." +
                            " Probably this is a default specter?", specterId))
                    .build();
        }
    }
}
